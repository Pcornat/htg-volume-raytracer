#include <iostream>
#include "HTGVolumeRaytracer.h"
#include "utils/commandHandler.h"
#include <thread>
#include <cstring>

#ifdef USE_OPENHTG
#include "openhtg/openHyperTreeGrid.h"
#include "openhtg/openHyperTreeGridCerealSerialization.h"
#endif

#ifdef USE_VTK
#include "vtkXMLHyperTreeGridReader.h"
#endif

typedef struct
{
	std::string filename;
	std::string fieldname;
	std::string scriptname;
} param_t;

#ifdef USE_INTERACTIVE_RENDERING_OPENGL

void rendering()
{
	HTGVolumeRaytracer::renderHTG();
	HTGVolumeRaytracer::destroyAndExit();
}

void cli()
{
	std::cout << "Type your commands here" << std::endl;
	commandHandler::getCommand<std::basic_istream<char>>(std::cin);
}

#endif

void getParameters(int argc, char **argv, param_t &param)
{
	unsigned i = 0;
	while (++i < argc)
	{
		if (!strcmp("-f", argv[i]))
		{
			param.filename = argv[++i];
			continue;
		}
		if (!strcmp("-d", argv[i]))
		{
			param.fieldname = argv[++i];
			continue;
		}
		if (!strcmp("-s", argv[i]))
		{
			param.scriptname = argv[++i];
			continue;
		}
	}
}

int main(int argc, char *argv[])
{
	auto begin = std::chrono::high_resolution_clock::now();
	auto end = begin;
	auto ioTime = begin;

	param_t param;
	getParameters(argc, argv, param);
	if (param.filename.empty())
	{
		std::cout << "parameters : \n"
					 "-f [ohtg file]\n"
					 "-d [scalar field to render] (optional)\n"
					 "-s [pre-processing script file] (optional)" << std::endl;
		return 0;
	}

	std::string ext = param.filename.substr(param.filename.find_last_of('.') + 1);
	HTGVolumeRaytracer::init();
	if(ext == "htg")
	{
#ifdef USE_VTK
		auto reader = vtkXMLHyperTreeGridReader::New();
		reader->SetFileName(param.filename.c_str());
		reader->Update();
		auto htg = reader->GetOutput();
		ioTime = std::chrono::high_resolution_clock::now();
		HTGVolumeRaytracer::setHTG(htg, true, param.fieldname.c_str());
#else
		std::cerr << "Error : VTK not linked to the program" << std::endl;
		exit(EXIT_FAILURE);
#endif

	}
	else if (ext == "ohtg" || ext == "shtg")
	{
#ifdef USE_OPENHTG
		openHyperTreeGrid *htg = new openHyperTreeGrid();
		openhtg::io::mcereal::loadFromFile(param.filename, *htg);
		ioTime = std::chrono::high_resolution_clock::now();
		if(htg->getNumberOfLevels() > HTG_RENDERING_MAX_LEVEL + 1)
		{
			std::cerr << "Warning : HTG number of levels higher than rendering max level, model might look coarser than it should.\n"
			             "You can adjust rendering max level in cmake config." << std::endl;
		}

		HTGVolumeRaytracer::setHTG(htg, false, param.fieldname.c_str());

#else
		std::cerr << "Error : OpenHTG not linked to the program" << std::endl;
		exit(EXIT_FAILURE);
#endif
	}
	else
	{
		std::cerr << "Error : unrecognized file extension" << std::endl;
		exit(EXIT_FAILURE);
	}



	end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsed = ioTime - begin;
	std::cout << "IO Time : " << elapsed.count() << " seconds" << std::endl;

	elapsed = end - ioTime;
	std::cout << "Pre-processing time : " << elapsed.count() << " seconds" << std::endl;

	elapsed = end - begin;
	std::cout << "Total time : " << elapsed.count() << " seconds" << std::endl;

//	rendering();

	if (!param.scriptname.empty())
	{
		std::ifstream script(param.scriptname);
		commandHandler::getCommand<std::ifstream>(script);
	}
#ifdef USE_INTERACTIVE_RENDERING_OPENGL

	std::thread renderingThread(rendering);
	std::thread cliThread(cli);

	renderingThread.join();
	cliThread.join();

#endif

	HTGVolumeRaytracer::destroyAndExit();
	return 0;
}
