//
// Created by rochea on 23/07/2020.
//

#ifndef HTGVOLUMERAYTRACER_CUDADEFINITIONS_H
#define HTGVOLUMERAYTRACER_CUDADEFINITIONS_H


#ifdef OPENHTG_USE_CUDA
#define OHTG_HOST_DEVICE __host__ __device__
#define OHTG_HOST __host__
#define OHTG_DEVICE __device__
#define OHTG_GLOBAL __global__
#else
#define OHTG_HOST_DEVICE
#define OHTG_HOST
#define OHTG_DEVICE
#define OHTG_GLOBAL
#endif

#endif //HTGVOLUMERAYTRACER_CUDADEFINITIONS_H
