//
// Created by antoine on 29/06/2020.
//

#ifndef HTGVOLUMERAYTRACER_HOST_UTILS_H
#define HTGVOLUMERAYTRACER_HOST_UTILS_H

#define M_PIf 3.14159265358979323846f

#include "floatVec3.h"
#include <cmath>

void calculateCameraVariables(camera_t & camera)
{
	float ulen, vlen, wlen;
	camera.W = camera.look_at - camera.eye;

	wlen = sqrtf(camera.W.dot(camera.W));
	camera.U = camera.W.cross(camera.up).normalize();
	camera.V = camera.U.cross(camera.W).normalize();

	ulen = wlen * tanf(0.5f * 90 * M_PIf / 180.0f);
	camera.U *= ulen;
	vlen = ulen / camera.ratio;
	camera.V *= vlen;

}

#endif //HTGVOLUMERAYTRACER_HOST_UTILS_H
