//
// Created by rochea on 15/07/2020.
//

#include "commandHandler.h"
#include <sstream>
#include <iostream>
#include <ctime>

#define MISS_ARGUMENT() \
{\
    std::cerr << "Error : missing argument" << std::endl; \
    return;\
}

#define GET_ARGUMENT_STREAM(stream, target) \
{\
    if(!(ss >> target))\
        MISS_ARGUMENT()\
}


void commandHandler::getCommandTransfer(std::stringstream &ss)
{
	std::string buffer;

	GET_ARGUMENT_STREAM(ss, buffer)
	if (buffer == "primary")
	{
		GET_ARGUMENT_STREAM(ss, buffer)
		if (buffer == "add")
		{
			float key, value;
			GET_ARGUMENT_STREAM(ss, key);
			GET_ARGUMENT_STREAM(ss, value);
			HTGVolumeRaytracer::transferFunctionAddPoint(true, key, value);
			return;
		}
		if (buffer == "remove")
		{
			float key;
			GET_ARGUMENT_STREAM(ss, key);
			HTGVolumeRaytracer::transferFunctionRemovePoint(true, key);
			return;
		}
		if (buffer == "min")
		{
			GET_ARGUMENT_STREAM(ss, buffer)
			if(buffer == "set")
			{
				float value;
				GET_ARGUMENT_STREAM(ss, value);
				HTGVolumeRaytracer::transferFunctionSetMinMax(true, true, value);
				return;
			}
			if(buffer == "remove")
			{
				HTGVolumeRaytracer::transferFunctionRemoveMinMax(true, true);
				return;
			}
		}
		if (buffer == "max")
		{
			GET_ARGUMENT_STREAM(ss, buffer)
			if(buffer == "set")
			{
				float value;
				GET_ARGUMENT_STREAM(ss, value);
				HTGVolumeRaytracer::transferFunctionSetMinMax(false, true, value);
				return;
			}

			if(buffer == "remove")
			{
				HTGVolumeRaytracer::transferFunctionRemoveMinMax(false, true);
				return;
			}
		}
		if (buffer == "reset")
		{
			HTGVolumeRaytracer::transferFunctionReset(true);
			return;
		}
		if (buffer == "info")
		{
			HTGVolumeRaytracer::transferFunctionShow(true);
			return;
		}

	}

	if (buffer == "secondary")
	{
		GET_ARGUMENT_STREAM(ss, buffer)
		if (buffer == "add")
		{
			float key, value;
			GET_ARGUMENT_STREAM(ss, key);
			GET_ARGUMENT_STREAM(ss, value);
			HTGVolumeRaytracer::transferFunctionAddPoint(false, key, value);
			return;
		}
		if (buffer == "remove")
		{
			float key;
			GET_ARGUMENT_STREAM(ss, key);
			HTGVolumeRaytracer::transferFunctionRemovePoint(false, key);
			return;
		}
		if (buffer == "min")
		{
			GET_ARGUMENT_STREAM(ss, buffer)
			if(buffer == "set")
			{
				float value;
				GET_ARGUMENT_STREAM(ss, value);
				HTGVolumeRaytracer::transferFunctionSetMinMax(true, false, value);
				return;
			}
			if(buffer == "remove")
			{
				HTGVolumeRaytracer::transferFunctionRemoveMinMax(true, false);
				return;
			}
		}
		if (buffer == "max")
		{
			GET_ARGUMENT_STREAM(ss, buffer)
			if(buffer == "set")
			{
				float value;
				GET_ARGUMENT_STREAM(ss, value);
				HTGVolumeRaytracer::transferFunctionSetMinMax(false, false, value);
				return;
			}
			if(buffer == "remove")
			{
				HTGVolumeRaytracer::transferFunctionRemoveMinMax(false, false);
				return;
			}
		}
		if (buffer == "reset")
		{
			HTGVolumeRaytracer::transferFunctionReset(false);
			return;
		}
		if (buffer == "info")
		{
			HTGVolumeRaytracer::transferFunctionShow(false);
			return;
		}
	}
	std::cerr << "Error : unknown " << buffer << " argument" << std::endl;
}

void commandHandler::getCommandScalar(std::stringstream &ss)
{
	std::string buffer;
	GET_ARGUMENT_STREAM(ss, buffer)

	if (buffer == "color")
	{
		GET_ARGUMENT_STREAM(ss, buffer)
		if (buffer == "set")
		{
			GET_ARGUMENT_STREAM(ss, buffer)
			if (!HTGVolumeRaytracer::setScalarToShow(buffer.c_str()))
			{
				std::cerr << "Error : scalar '" << buffer << "' does not exist" << std::endl;
			}
			return;
		}
		if (buffer == "contribution")
		{
			GET_ARGUMENT_STREAM(ss, buffer)
			if (buffer == "on")
			{
				HTGVolumeRaytracer::setScalarContributionMode(true);
				return;
			}
			if (buffer == "off")
			{
				HTGVolumeRaytracer::setScalarContributionMode(false);
				return;
			}
		}
		if (buffer == "info")
		{
			HTGVolumeRaytracer::displayScalarInfo(true);
			return;
		}
		if (buffer == "min")
		{
			float value;
			GET_ARGUMENT_STREAM(ss, value)
			HTGVolumeRaytracer::setMinMaxScalar(true, value);
			return;
		}
		if (buffer == "max")
		{
			float value;
			GET_ARGUMENT_STREAM(ss, value)
			HTGVolumeRaytracer::setMinMaxScalar(false, value);
			return;
		}
		if (buffer == "reset")
		{
			HTGVolumeRaytracer::resetMinMaxScalar(true);
			return;
		}
	}
	if (buffer == "opacity")
	{
		GET_ARGUMENT_STREAM(ss, buffer)
		if (buffer == "set")
		{
			GET_ARGUMENT_STREAM(ss, buffer)
			if (!HTGVolumeRaytracer::setScalarOpacity(buffer.c_str()))
			{
				std::cerr << "Error : scalar '" << buffer << "' does not exist" << std::endl;
			}

			return;
		}
		if (buffer == "on")
		{
			if (!HTGVolumeRaytracer::setScalarOpacityMode(true))
			{
				std::cerr << "Error : no scalar has been set for opacity" << std::endl;
			}
			return;
		}
		if (buffer == "off")
		{
			HTGVolumeRaytracer::setScalarOpacityMode(false);
			return;
		}
		if (buffer == "info")
		{
			HTGVolumeRaytracer::displayScalarInfo(false);
			return;
		}

	}
	if (buffer == "list")
	{
		HTGVolumeRaytracer::displayScalars();
		return;
	}
	std::cerr << "Error : unknown " << buffer << " argument" << std::endl;

}

void commandHandler::getCommandCamera(std::stringstream &ss)
{
	std::string buffer;
	GET_ARGUMENT_STREAM(ss, buffer)
	if (buffer == "rotate")
	{
		float x, y;

		GET_ARGUMENT_STREAM(ss, x)
		GET_ARGUMENT_STREAM(ss, y)

		HTGVolumeRaytracer::rotate(x, y);
		return;
	}
	if (buffer == "move")
	{
		float x, y;

		GET_ARGUMENT_STREAM(ss, x)
		GET_ARGUMENT_STREAM(ss, y)

		HTGVolumeRaytracer::move(x, y);
		return;
	}
	if (buffer == "zoom")
	{
		float l;
		GET_ARGUMENT_STREAM(ss, l)
		HTGVolumeRaytracer::zoom(l);
		return;
	}
	if (buffer == "info")
	{
		HTGVolumeRaytracer::displayCameraInfo();
		return;
	}
	if (buffer == "position")
	{
		float x, y, z;
		GET_ARGUMENT_STREAM(ss, x);
		GET_ARGUMENT_STREAM(ss, y);
		GET_ARGUMENT_STREAM(ss, z);

		HTGVolumeRaytracer::setCameraPosition(x, y, z);
		return;
	}
	if (buffer == "lookat")
	{
		float x, y, z;
		GET_ARGUMENT_STREAM(ss, x);
		GET_ARGUMENT_STREAM(ss, y);
		GET_ARGUMENT_STREAM(ss, z);

		HTGVolumeRaytracer::setCameraLookAt(x, y, z);
		return;
	}

	std::cerr << "Error : unknown " << buffer << " argument" << std::endl;
}

void commandHandler::getCommandScreenShot(std::stringstream &ss)
{
	unsigned width, height;
	std::string buffer;
	GET_ARGUMENT_STREAM(ss, width)
	GET_ARGUMENT_STREAM(ss, height)

	time_t rawtime;
	struct tm *timeinfo;
	char timeBuffer[80];

	time(&rawtime);
	timeinfo = localtime(&rawtime);
	std::string filename;
	if (ss >> buffer)
	{
		filename = buffer;
	}
	else
	{
		strftime(timeBuffer, sizeof(timeBuffer), "%Y-%m-%d_%H:%M:%S", timeinfo);
		filename = "image_";
		std::string nbScreenString = std::to_string(++nbScreenShots);
		filename += timeBuffer;
		filename += ".jpg";
	}


	HTGVolumeRaytracer::screenShotNewKernel(width, height, filename.c_str());

}

template<class streamType>
void commandHandler::getCommand(streamType &stream)
{

	char tmpBuffer[128];
	while (stream.getline(tmpBuffer, 128))
	{
		std::stringstream ss;
		std::string buffer = tmpBuffer;

		ss << buffer;
		ss >> buffer;
		if (buffer == "scalar")
		{
			getCommandScalar(ss);
			continue;
		}
		if (buffer == "screenshot")
		{
			getCommandScreenShot(ss);
			continue;
		}
		if (buffer == "camera")
		{
			getCommandCamera(ss);
			continue;
		}

		if (buffer == "transfer")
		{
			getCommandTransfer(ss);
			continue;
		}

		if (buffer == "exit")
		{
			HTGVolumeRaytracer::destroyAndExit();
		}

		if (buffer == "help")
		{
			displayHelp();
			continue;
		}

		std::cerr << "Unknown command :'" << buffer << "'" << std::endl;

	}
}

void commandHandler::displayHelp()
{
	std::cout <<
			  "Command list :\n"
			  "\nscalar\n"

			  "  color\n"
			  "    set [scalarfield name] : Set the scalar field to colorize with\n"
			  "    contribution [on / off] : Enable/disable opacity contribution from the color scalar\n"
			  "    info : display current scalar info\n"
			  "    min [value] : set the minimum value for the lookup table\n"
			  "    max [value] : set the maximum value for the lookup table\n"
			  "    reset : reset min and max for the lookup table\n"

			  "  opacity\n"
			  "    set [scalarfield name] : Set the scalar field to make opacity with\n"
			  "    on/off : Enable/disable opacity contribution from the secondary scalar\n"
			  "    info : display current scalar info\n"

			  "transfer\n"
			  "  primary\n"
			  "    add [key] [value] : Add a point in the primary transfer function\n"
			  "    remove [key] : Remove a point in the primary transfer function\n"
			  "    min/max [value] : Set the value for the min/max in the primary transfer function\n"
			  "    info : Show primary transfer function info\n"
			  "    reset : Reset the primary transfer function\n"

			  "  secondary\n"
			  "    add [key] [value] : Add a point in the secondary transfer function\n"
			  "    remove [key] : Remove a point in the secondary transfer function\n"
			  "    min/max [value] : Set the value for the min/max in the secondary transfer function\n"
			  "    info : Show secondary transfer function info\n"
			  "    reset : Reset the secondary transfer function\n"

			  "\ncamera\n"
			  "  move [x] [y] : move the camera without rotate it\n"
			  "  rotate [x] [y] : rotate camera around the lookAt point\n"
			  "  zoom [value] : zoom\n"
			  "  info : display camera info\n"
			  "  position [x] [y] [z] : set the camera at the x,y,z position\n"
			  "  lookat [x] [y] [z] : make the camera look at the x,y,z position\n"

			  "\nscreenshot [width] [height] : make a screenshot from the current pov and write it in file\n"
			  "help : Display this text\n"
			  "exit : exit the program\n"

			  << std::endl;

}
